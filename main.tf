terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">=5.31.0"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=3.87.0"
    }
    google = {
      source  = "hashicorp/google"
      version = ">=5.11.0"
    }
    tfe = {
      source  = "hashicorp/tfe"
      version = ">=0.42.0"
    }
  }
}

provider "azurerm" {
  features {}
}
provider "tfe" {
  //  organization = "danny-hashicorp"
}


