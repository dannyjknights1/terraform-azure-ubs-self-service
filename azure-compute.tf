resource "azurerm_resource_group" "ubs_resource_group" {
  name     = "ubs-resources-demo"
  location = "UK South"
}

resource "azurerm_virtual_network" "ubs_virtual_network" {
  name                = "ubs-virtual-network-demo"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.ubs_resource_group.location
  resource_group_name = azurerm_resource_group.ubs_resource_group.name
}

resource "azurerm_subnet" "ubs_subnet" {
  name                 = "ubs-internal-subnet"
  resource_group_name  = azurerm_resource_group.ubs_resource_group.name
  virtual_network_name = azurerm_virtual_network.ubs_virtual_network.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_interface" "ubs_ni" {
  name                = "ubs-nic"
  location            = azurerm_resource_group.ubs_resource_group.location
  resource_group_name = azurerm_resource_group.ubs_resource_group.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.ubs_subnet.id
    private_ip_address_allocation = "Dynamic"

  }
}

data "azurerm_virtual_machine" "ubs_vm" {
  name                = azurerm_linux_virtual_machine.ubs_vm.name
  resource_group_name = azurerm_resource_group.ubs_resource_group.name
}

check "check_vm_state" {
  assert {
    condition = data.azurerm_virtual_machine.ubs_vm.power_state == "running"
    error_message = format("Virtual Machine (%s) should be in a 'running' status, instead state is '%s'",
      data.azurerm_virtual_machine.ubs_vm.id,
      data.azurerm_virtual_machine.ubs_vm.power_state
    )
  }
}

resource "azurerm_linux_virtual_machine" "ubs_vm" {
  name                            = "ubs-linux-machine-demo"
  resource_group_name             = azurerm_resource_group.ubs_resource_group.name
  location                        = azurerm_resource_group.ubs_resource_group.location
  size                            = "Standard_B1ls"
  admin_username                  = "adminuser"
  admin_password                  = "H@shiC0rp!"
  disable_password_authentication = false
  network_interface_ids = [
    azurerm_network_interface.ubs_ni.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }
}