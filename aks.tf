resource "azurerm_kubernetes_cluster" "ubs_aks_cluster" {
  name                = "ubs-aks1"
  location            = azurerm_resource_group.ubs_resource_group.location
  resource_group_name = azurerm_resource_group.ubs_resource_group.name
  dns_prefix          = "ubsaks1"

  default_node_pool {
    name       = "ubsaksdemo"
    node_count = 1
    vm_size    = "Standard_D2_v2"
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    Environment = "Production"
  }
}